<?xml version="1.0" encoding="utf-8" standalone="no"?>
<book xmlns="http://www.devhelp.net/book" title="CTPL Reference Manual" link="index.html" author="" name="ctpl" version="2" language="c" online="http://ctpl.tuxfamily.org/doc/unstable/index.html">
  <chapters>
    <sub name="CTPL overview" link="ch01.html">
      <sub name="Introduction" link="ch01.html#id-1.2.2">
        <sub name="Advantages and disadvantages" link="ch01.html#id-1.2.2.4">
          <sub name="Advantages" link="ch01.html#id-1.2.2.4.2"/>
          <sub name="Disadvantages" link="ch01.html#id-1.2.2.4.3"/>
        </sub>
      </sub>
      <sub name="Working design" link="ch01s02.html">
        <sub name="The lexer" link="ch01s02.html#id-1.2.3.3"/>
        <sub name="The parser" link="ch01s02.html#id-1.2.3.4"/>
      </sub>
      <sub name="Templates syntax" link="ch01s03.html">
        <sub name="Raw data" link="ch01s03.html#raw-data"/>
        <sub name="Template blocks" link="ch01s03.html#template-blocks"/>
        <sub name="Examples" link="ch01s03.html#id-1.2.4.5"/>
      </sub>
      <sub name="Environment description syntax" link="environment-description-syntax.html">
        <sub name="Symbol" link="environment-description-syntax.html#environment-description-syntax-symbol"/>
        <sub name="Value" link="environment-description-syntax.html#environment-description-syntax-value"/>
        <sub name="Blanks" link="environment-description-syntax.html#environment-description-syntax-blank"/>
        <sub name="Comments" link="environment-description-syntax.html#environment-description-syntax-comment"/>
      </sub>
      <sub name="Input and output encoding" link="ch01s05.html"/>
    </sub>
    <sub name="API reference" link="ch02.html">
      <sub name="Version information" link="ctpl-Version-information.html"/>
      <sub name="CtplValue" link="ctpl-CtplValue.html"/>
      <sub name="CtplEnviron" link="ctpl-CtplEnviron.html"/>
      <sub name="CtplToken" link="ctpl-CtplToken.html"/>
      <sub name="CtplLexer" link="ctpl-CtplLexer.html"/>
      <sub name="CtplLexerExpr" link="ctpl-CtplLexerExpr.html"/>
      <sub name="CtplParser" link="ctpl-CtplParser.html"/>
      <sub name="CtplEval" link="ctpl-CtplEval.html"/>
      <sub name="Generic IO" link="ctpl-Generic-IO.html"/>
      <sub name="CtplInputStream" link="ctpl-CtplInputStream.html"/>
      <sub name="CtplOutputStream" link="ctpl-CtplOutputStream.html"/>
    </sub>
    <sub name="API Index" link="api-index-full.html"/>
    <sub name="Deprecated API Index" link="api-index-deprecated.html"/>
    <sub name="Index of new symbols in 0.2" link="api-index-0-2.html"/>
    <sub name="Index of new symbols in 0.3" link="api-index-0-3.html"/>
    <sub name="Annotation Glossary" link="annotation-glossary.html"/>
  </chapters>
  <functions>
    <keyword type="macro" name="CTPL_CHECK_VERSION()" link="ctpl-Version-information.html#CTPL-CHECK-VERSION:CAPS" since="0.3"/>
    <keyword type="function" name="ctpl_check_version ()" link="ctpl-Version-information.html#ctpl-check-version" since="0.3"/>
    <keyword type="macro" name="CTPL_MAJOR_VERSION" link="ctpl-Version-information.html#CTPL-MAJOR-VERSION:CAPS" since="0.3"/>
    <keyword type="macro" name="CTPL_MINOR_VERSION" link="ctpl-Version-information.html#CTPL-MINOR-VERSION:CAPS" since="0.3"/>
    <keyword type="macro" name="CTPL_MICRO_VERSION" link="ctpl-Version-information.html#CTPL-MICRO-VERSION:CAPS" since="0.3"/>
    <keyword type="variable" name="ctpl_major_version" link="ctpl-Version-information.html#ctpl-major-version" since="0.3"/>
    <keyword type="variable" name="ctpl_minor_version" link="ctpl-Version-information.html#ctpl-minor-version" since="0.3"/>
    <keyword type="variable" name="ctpl_micro_version" link="ctpl-Version-information.html#ctpl-micro-version" since="0.3"/>
    <keyword type="macro" name="CTPL_VALUE_HOLDS()" link="ctpl-CtplValue.html#CTPL-VALUE-HOLDS:CAPS"/>
    <keyword type="macro" name="CTPL_VALUE_HOLDS_INT()" link="ctpl-CtplValue.html#CTPL-VALUE-HOLDS-INT:CAPS"/>
    <keyword type="macro" name="CTPL_VALUE_HOLDS_FLOAT()" link="ctpl-CtplValue.html#CTPL-VALUE-HOLDS-FLOAT:CAPS"/>
    <keyword type="macro" name="CTPL_VALUE_HOLDS_STRING()" link="ctpl-CtplValue.html#CTPL-VALUE-HOLDS-STRING:CAPS"/>
    <keyword type="macro" name="CTPL_VALUE_HOLDS_ARRAY()" link="ctpl-CtplValue.html#CTPL-VALUE-HOLDS-ARRAY:CAPS"/>
    <keyword type="function" name="ctpl_value_init ()" link="ctpl-CtplValue.html#ctpl-value-init"/>
    <keyword type="function" name="ctpl_value_new ()" link="ctpl-CtplValue.html#ctpl-value-new"/>
    <keyword type="function" name="ctpl_value_copy ()" link="ctpl-CtplValue.html#ctpl-value-copy"/>
    <keyword type="function" name="ctpl_value_dup ()" link="ctpl-CtplValue.html#ctpl-value-dup"/>
    <keyword type="function" name="ctpl_value_free_value ()" link="ctpl-CtplValue.html#ctpl-value-free-value"/>
    <keyword type="function" name="ctpl_value_free ()" link="ctpl-CtplValue.html#ctpl-value-free"/>
    <keyword type="function" name="ctpl_value_new_int ()" link="ctpl-CtplValue.html#ctpl-value-new-int"/>
    <keyword type="function" name="ctpl_value_new_float ()" link="ctpl-CtplValue.html#ctpl-value-new-float"/>
    <keyword type="function" name="ctpl_value_new_string ()" link="ctpl-CtplValue.html#ctpl-value-new-string"/>
    <keyword type="function" name="ctpl_value_new_arrayv ()" link="ctpl-CtplValue.html#ctpl-value-new-arrayv"/>
    <keyword type="function" name="ctpl_value_new_array ()" link="ctpl-CtplValue.html#ctpl-value-new-array"/>
    <keyword type="function" name="ctpl_value_set_int ()" link="ctpl-CtplValue.html#ctpl-value-set-int"/>
    <keyword type="function" name="ctpl_value_set_float ()" link="ctpl-CtplValue.html#ctpl-value-set-float"/>
    <keyword type="function" name="ctpl_value_set_string ()" link="ctpl-CtplValue.html#ctpl-value-set-string"/>
    <keyword type="function" name="ctpl_value_take_string ()" link="ctpl-CtplValue.html#ctpl-value-take-string"/>
    <keyword type="function" name="ctpl_value_set_arrayv ()" link="ctpl-CtplValue.html#ctpl-value-set-arrayv"/>
    <keyword type="function" name="ctpl_value_set_array ()" link="ctpl-CtplValue.html#ctpl-value-set-array"/>
    <keyword type="function" name="ctpl_value_set_array_intv ()" link="ctpl-CtplValue.html#ctpl-value-set-array-intv"/>
    <keyword type="function" name="ctpl_value_set_array_int ()" link="ctpl-CtplValue.html#ctpl-value-set-array-int"/>
    <keyword type="function" name="ctpl_value_set_array_floatv ()" link="ctpl-CtplValue.html#ctpl-value-set-array-floatv"/>
    <keyword type="function" name="ctpl_value_set_array_float ()" link="ctpl-CtplValue.html#ctpl-value-set-array-float"/>
    <keyword type="function" name="ctpl_value_set_array_stringv ()" link="ctpl-CtplValue.html#ctpl-value-set-array-stringv"/>
    <keyword type="function" name="ctpl_value_set_array_string ()" link="ctpl-CtplValue.html#ctpl-value-set-array-string"/>
    <keyword type="function" name="ctpl_value_array_append ()" link="ctpl-CtplValue.html#ctpl-value-array-append"/>
    <keyword type="function" name="ctpl_value_array_prepend ()" link="ctpl-CtplValue.html#ctpl-value-array-prepend"/>
    <keyword type="function" name="ctpl_value_array_append_int ()" link="ctpl-CtplValue.html#ctpl-value-array-append-int"/>
    <keyword type="function" name="ctpl_value_array_prepend_int ()" link="ctpl-CtplValue.html#ctpl-value-array-prepend-int"/>
    <keyword type="function" name="ctpl_value_array_append_float ()" link="ctpl-CtplValue.html#ctpl-value-array-append-float"/>
    <keyword type="function" name="ctpl_value_array_prepend_float ()" link="ctpl-CtplValue.html#ctpl-value-array-prepend-float"/>
    <keyword type="function" name="ctpl_value_array_append_string ()" link="ctpl-CtplValue.html#ctpl-value-array-append-string"/>
    <keyword type="function" name="ctpl_value_array_prepend_string ()" link="ctpl-CtplValue.html#ctpl-value-array-prepend-string"/>
    <keyword type="function" name="ctpl_value_array_length ()" link="ctpl-CtplValue.html#ctpl-value-array-length"/>
    <keyword type="function" name="ctpl_value_array_index ()" link="ctpl-CtplValue.html#ctpl-value-array-index"/>
    <keyword type="function" name="ctpl_value_get_held_type ()" link="ctpl-CtplValue.html#ctpl-value-get-held-type"/>
    <keyword type="function" name="ctpl_value_get_int ()" link="ctpl-CtplValue.html#ctpl-value-get-int"/>
    <keyword type="function" name="ctpl_value_get_float ()" link="ctpl-CtplValue.html#ctpl-value-get-float"/>
    <keyword type="function" name="ctpl_value_get_string ()" link="ctpl-CtplValue.html#ctpl-value-get-string"/>
    <keyword type="function" name="ctpl_value_get_array ()" link="ctpl-CtplValue.html#ctpl-value-get-array"/>
    <keyword type="function" name="ctpl_value_get_array_int ()" link="ctpl-CtplValue.html#ctpl-value-get-array-int"/>
    <keyword type="function" name="ctpl_value_get_array_float ()" link="ctpl-CtplValue.html#ctpl-value-get-array-float"/>
    <keyword type="function" name="ctpl_value_get_array_string ()" link="ctpl-CtplValue.html#ctpl-value-get-array-string"/>
    <keyword type="function" name="ctpl_value_to_string ()" link="ctpl-CtplValue.html#ctpl-value-to-string"/>
    <keyword type="function" name="ctpl_value_convert ()" link="ctpl-CtplValue.html#ctpl-value-convert"/>
    <keyword type="function" name="ctpl_value_type_get_name ()" link="ctpl-CtplValue.html#ctpl-value-type-get-name"/>
    <keyword type="macro" name="ctpl_value_get_held_type_name()" link="ctpl-CtplValue.html#ctpl-value-get-held-type-name"/>
    <keyword type="enum" name="enum CtplValueType" link="ctpl-CtplValue.html#CtplValueType"/>
    <keyword type="struct" name="struct CtplValue" link="ctpl-CtplValue.html#CtplValue"/>
    <keyword type="function" name="CtplEnvironForeachFunc ()" link="ctpl-CtplEnviron.html#CtplEnvironForeachFunc"/>
    <keyword type="function" name="ctpl_environ_new ()" link="ctpl-CtplEnviron.html#ctpl-environ-new"/>
    <keyword type="function" name="ctpl_environ_ref ()" link="ctpl-CtplEnviron.html#ctpl-environ-ref" since="0.3"/>
    <keyword type="function" name="ctpl_environ_unref ()" link="ctpl-CtplEnviron.html#ctpl-environ-unref" since="0.3"/>
    <keyword type="function" name="ctpl_environ_lookup ()" link="ctpl-CtplEnviron.html#ctpl-environ-lookup"/>
    <keyword type="function" name="ctpl_environ_push ()" link="ctpl-CtplEnviron.html#ctpl-environ-push"/>
    <keyword type="function" name="ctpl_environ_push_int ()" link="ctpl-CtplEnviron.html#ctpl-environ-push-int"/>
    <keyword type="function" name="ctpl_environ_push_float ()" link="ctpl-CtplEnviron.html#ctpl-environ-push-float"/>
    <keyword type="function" name="ctpl_environ_push_string ()" link="ctpl-CtplEnviron.html#ctpl-environ-push-string"/>
    <keyword type="function" name="ctpl_environ_pop ()" link="ctpl-CtplEnviron.html#ctpl-environ-pop" since="0.3"/>
    <keyword type="function" name="ctpl_environ_foreach ()" link="ctpl-CtplEnviron.html#ctpl-environ-foreach"/>
    <keyword type="function" name="ctpl_environ_merge ()" link="ctpl-CtplEnviron.html#ctpl-environ-merge"/>
    <keyword type="function" name="ctpl_environ_add_from_stream ()" link="ctpl-CtplEnviron.html#ctpl-environ-add-from-stream"/>
    <keyword type="function" name="ctpl_environ_add_from_path ()" link="ctpl-CtplEnviron.html#ctpl-environ-add-from-path"/>
    <keyword type="function" name="ctpl_environ_add_from_string ()" link="ctpl-CtplEnviron.html#ctpl-environ-add-from-string"/>
    <keyword type="macro" name="CTPL_ENVIRON_ERROR" link="ctpl-CtplEnviron.html#CTPL-ENVIRON-ERROR:CAPS"/>
    <keyword type="enum" name="enum CtplEnvironError" link="ctpl-CtplEnviron.html#CtplEnvironError"/>
    <keyword type="struct" name="CtplEnviron" link="ctpl-CtplEnviron.html#CtplEnviron"/>
    <keyword type="function" name="ctpl_token_free ()" link="ctpl-CtplToken.html#ctpl-token-free"/>
    <keyword type="function" name="ctpl_token_expr_free ()" link="ctpl-CtplToken.html#ctpl-token-expr-free"/>
    <keyword type="struct" name="CtplToken" link="ctpl-CtplToken.html#CtplToken"/>
    <keyword type="struct" name="CtplTokenExpr" link="ctpl-CtplToken.html#CtplTokenExpr"/>
    <keyword type="function" name="ctpl_lexer_lex ()" link="ctpl-CtplLexer.html#ctpl-lexer-lex"/>
    <keyword type="function" name="ctpl_lexer_lex_string ()" link="ctpl-CtplLexer.html#ctpl-lexer-lex-string"/>
    <keyword type="function" name="ctpl_lexer_lex_path ()" link="ctpl-CtplLexer.html#ctpl-lexer-lex-path"/>
    <keyword type="macro" name="CTPL_LEXER_ERROR" link="ctpl-CtplLexer.html#CTPL-LEXER-ERROR:CAPS"/>
    <keyword type="enum" name="enum CtplLexerError" link="ctpl-CtplLexer.html#CtplLexerError"/>
    <keyword type="function" name="ctpl_lexer_expr_lex ()" link="ctpl-CtplLexerExpr.html#ctpl-lexer-expr-lex"/>
    <keyword type="function" name="ctpl_lexer_expr_lex_full ()" link="ctpl-CtplLexerExpr.html#ctpl-lexer-expr-lex-full"/>
    <keyword type="function" name="ctpl_lexer_expr_lex_string ()" link="ctpl-CtplLexerExpr.html#ctpl-lexer-expr-lex-string"/>
    <keyword type="macro" name="CTPL_LEXER_EXPR_ERROR" link="ctpl-CtplLexerExpr.html#CTPL-LEXER-EXPR-ERROR:CAPS"/>
    <keyword type="enum" name="enum CtplLexerExprError" link="ctpl-CtplLexerExpr.html#CtplLexerExprError"/>
    <keyword type="function" name="ctpl_parser_parse ()" link="ctpl-CtplParser.html#ctpl-parser-parse"/>
    <keyword type="macro" name="CTPL_PARSER_ERROR" link="ctpl-CtplParser.html#CTPL-PARSER-ERROR:CAPS"/>
    <keyword type="enum" name="enum CtplParserError" link="ctpl-CtplParser.html#CtplParserError"/>
    <keyword type="function" name="ctpl_eval_value ()" link="ctpl-CtplEval.html#ctpl-eval-value" since="0.2"/>
    <keyword type="function" name="ctpl_eval_bool ()" link="ctpl-CtplEval.html#ctpl-eval-bool" since="0.2"/>
    <keyword type="macro" name="CTPL_EVAL_ERROR" link="ctpl-CtplEval.html#CTPL-EVAL-ERROR:CAPS"/>
    <keyword type="enum" name="enum CtplEvalError" link="ctpl-CtplEval.html#CtplEvalError"/>
    <keyword type="enum" name="enum CtplIOError" link="ctpl-Generic-IO.html#CtplIOError"/>
    <keyword type="macro" name="CTPL_IO_ERROR" link="ctpl-Generic-IO.html#CTPL-IO-ERROR:CAPS"/>
    <keyword type="function" name="ctpl_input_stream_new ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-new" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_new_for_gfile ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-new-for-gfile" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_new_for_memory ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-new-for-memory" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_new_for_path ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-new-for-path" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_new_for_uri ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-new-for-uri" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_ref ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-ref" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_unref ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-unref" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_get_stream ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-get-stream" since="0.3"/>
    <keyword type="function" name="ctpl_input_stream_get_name ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-get-name" since="0.3"/>
    <keyword type="function" name="ctpl_input_stream_get_line ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-get-line" since="0.3"/>
    <keyword type="function" name="ctpl_input_stream_get_line_position ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-get-line-position" since="0.3"/>
    <keyword type="function" name="ctpl_input_stream_set_error ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-set-error" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_get_c ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-get-c" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_float ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-float" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_int ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-int" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_number ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-number" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_string_literal ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-string-literal" since="0.2"/>
    <keyword type="macro" name="ctpl_input_stream_read_symbol()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-symbol" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_symbol_full ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-symbol-full" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_read_word ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-read-word" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_peek ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-peek" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_peek_c ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-peek-c" since="0.2"/>
    <keyword type="macro" name="ctpl_input_stream_peek_symbol()" link="ctpl-CtplInputStream.html#ctpl-input-stream-peek-symbol" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_peek_symbol_full ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-peek-symbol-full" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_peek_word ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-peek-word" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_skip ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-skip" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_skip_blank ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-skip-blank" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_skip_word ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-skip-word" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_eof ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-eof" since="0.2"/>
    <keyword type="function" name="ctpl_input_stream_eof_fast ()" link="ctpl-CtplInputStream.html#ctpl-input-stream-eof-fast"/>
    <keyword type="macro" name="CTPL_EOF" link="ctpl-CtplInputStream.html#CTPL-EOF:CAPS" since="0.2"/>
    <keyword type="struct" name="CtplInputStream" link="ctpl-CtplInputStream.html#CtplInputStream"/>
    <keyword type="function" name="ctpl_output_stream_new ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-new" since="0.2"/>
    <keyword type="function" name="ctpl_output_stream_ref ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-ref" since="0.2"/>
    <keyword type="function" name="ctpl_output_stream_unref ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-unref" since="0.2"/>
    <keyword type="function" name="ctpl_output_stream_get_stream ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-get-stream" since="0.3"/>
    <keyword type="function" name="ctpl_output_stream_write ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-write" since="0.2"/>
    <keyword type="function" name="ctpl_output_stream_put_c ()" link="ctpl-CtplOutputStream.html#ctpl-output-stream-put-c" since="0.2"/>
    <keyword type="struct" name="CtplOutputStream" link="ctpl-CtplOutputStream.html#CtplOutputStream"/>
    <keyword type="constant" name="CTPL_VTYPE_INT" link="ctpl-CtplValue.html#CTPL-VTYPE-INT:CAPS"/>
    <keyword type="constant" name="CTPL_VTYPE_FLOAT" link="ctpl-CtplValue.html#CTPL-VTYPE-FLOAT:CAPS"/>
    <keyword type="constant" name="CTPL_VTYPE_STRING" link="ctpl-CtplValue.html#CTPL-VTYPE-STRING:CAPS"/>
    <keyword type="constant" name="CTPL_VTYPE_ARRAY" link="ctpl-CtplValue.html#CTPL-VTYPE-ARRAY:CAPS"/>
    <keyword type="constant" name="CTPL_ENVIRON_ERROR_LOADER_MISSING_SYMBOL" link="ctpl-CtplEnviron.html#CTPL-ENVIRON-ERROR-LOADER-MISSING-SYMBOL:CAPS"/>
    <keyword type="constant" name="CTPL_ENVIRON_ERROR_LOADER_MISSING_VALUE" link="ctpl-CtplEnviron.html#CTPL-ENVIRON-ERROR-LOADER-MISSING-VALUE:CAPS"/>
    <keyword type="constant" name="CTPL_ENVIRON_ERROR_LOADER_MISSING_SEPARATOR" link="ctpl-CtplEnviron.html#CTPL-ENVIRON-ERROR-LOADER-MISSING-SEPARATOR:CAPS"/>
    <keyword type="constant" name="CTPL_ENVIRON_ERROR_FAILED" link="ctpl-CtplEnviron.html#CTPL-ENVIRON-ERROR-FAILED:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_ERROR_SYNTAX_ERROR" link="ctpl-CtplLexer.html#CTPL-LEXER-ERROR-SYNTAX-ERROR:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_ERROR_FAILED" link="ctpl-CtplLexer.html#CTPL-LEXER-ERROR-FAILED:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_EXPR_ERROR_MISSING_OPERAND" link="ctpl-CtplLexerExpr.html#CTPL-LEXER-EXPR-ERROR-MISSING-OPERAND:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_EXPR_ERROR_MISSING_OPERATOR" link="ctpl-CtplLexerExpr.html#CTPL-LEXER-EXPR-ERROR-MISSING-OPERATOR:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_EXPR_ERROR_SYNTAX_ERROR" link="ctpl-CtplLexerExpr.html#CTPL-LEXER-EXPR-ERROR-SYNTAX-ERROR:CAPS"/>
    <keyword type="constant" name="CTPL_LEXER_EXPR_ERROR_FAILED" link="ctpl-CtplLexerExpr.html#CTPL-LEXER-EXPR-ERROR-FAILED:CAPS"/>
    <keyword type="constant" name="CTPL_PARSER_ERROR_INCOMPATIBLE_SYMBOL" link="ctpl-CtplParser.html#CTPL-PARSER-ERROR-INCOMPATIBLE-SYMBOL:CAPS"/>
    <keyword type="constant" name="CTPL_PARSER_ERROR_SYMBOL_NOT_FOUND" link="ctpl-CtplParser.html#CTPL-PARSER-ERROR-SYMBOL-NOT-FOUND:CAPS"/>
    <keyword type="constant" name="CTPL_PARSER_ERROR_FAILED" link="ctpl-CtplParser.html#CTPL-PARSER-ERROR-FAILED:CAPS"/>
    <keyword type="constant" name="CTPL_EVAL_ERROR_INVALID_OPERAND" link="ctpl-CtplEval.html#CTPL-EVAL-ERROR-INVALID-OPERAND:CAPS"/>
    <keyword type="constant" name="CTPL_EVAL_ERROR_SYMBOL_NOT_FOUND" link="ctpl-CtplEval.html#CTPL-EVAL-ERROR-SYMBOL-NOT-FOUND:CAPS"/>
    <keyword type="constant" name="CTPL_EVAL_ERROR_FAILED" link="ctpl-CtplEval.html#CTPL-EVAL-ERROR-FAILED:CAPS"/>
    <keyword type="constant" name="CTPL_IO_ERROR_EOF" link="ctpl-Generic-IO.html#CTPL-IO-ERROR-EOF:CAPS"/>
    <keyword type="constant" name="CTPL_IO_ERROR_INVALID_NUMBER" link="ctpl-Generic-IO.html#CTPL-IO-ERROR-INVALID-NUMBER:CAPS"/>
    <keyword type="constant" name="CTPL_IO_ERROR_INVALID_STRING" link="ctpl-Generic-IO.html#CTPL-IO-ERROR-INVALID-STRING:CAPS"/>
    <keyword type="constant" name="CTPL_IO_ERROR_RANGE" link="ctpl-Generic-IO.html#CTPL-IO-ERROR-RANGE:CAPS"/>
    <keyword type="constant" name="CTPL_IO_ERROR_FAILED" link="ctpl-Generic-IO.html#CTPL-IO-ERROR-FAILED:CAPS"/>
  </functions>
</book>
